<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Derucode Indonesia</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url();?>assets/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=base_url();?>assets/theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>assets/theme/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=base_url();?>assets/theme/css/stylish-portfolio.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
          <a class="js-scroll-trigger" href="#page-top">Derucode</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#page-top">Home</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#about">About</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#services">Services</a>
        </li>
		<!--
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#portfolio">Portfolio</a>
        </li>
		-->
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#contact">Contact</a>
        </li>
      </ul>
    </nav>

    <!-- Header -->
    <header class="masthead d-flex">
      <div class="container text-center my-auto">
        <!--
		<a class="js-scroll-trigger" href="#about"><img width="200px;" src="<?=base_url();?>assets/theme/img/logo.jpg"></a>
		-->
      </div>
      <div class="overlay"></div>
    </header>

    <!-- About -->
    <section class="content-section bg-light" id="about">
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>DERUCODE ASRIFIA INDONESIA</h2>
            <p class="lead mb-5">We are here to help people and organizations to transforms the way they operate, make their work easier, increase their efficiency, and support a better decision making with technology</p>
            <a class="btn btn-dark btn-xl js-scroll-trigger" href="#services">What We Offer</a>
          </div>
        </div>
      </div>
    </section>

    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
        <div class="content-section-heading">
          <h3 class="text-secondary mb-0">Services</h3>
          <h2 class="mb-5">What We Offer</h2>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <i class="fa fa-city"></i>
            </span>
            <h4>
              <strong>IT Business Development</strong>
            </h4>
            <p class="text-faded mb-0">Analyze your business for IT Solution</p>
          </div>
          <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <i class="fa fa-cogs"></i>
            </span>
            <h4>
              <strong>IT Maintenance</strong>
            </h4>
            <p class="text-faded mb-0">Resources support for your need, hardware, network and information system</p>
          </div>
          <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <i class="fa fa-laptop-code"></i>
            </span>
            <h4>
              <strong>Software Production</strong>
            </h4>
            <p class="text-faded mb-0">Web, Desktop or Mobile Apps</p>
          </div>
          <div class="col-lg-3 col-md-6">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <i class="fa fa-book"></i>
            </span>
            <h4>
              <strong>IT Consultation &amp; Private</strong>
            </h4>
            <p class="text-faded mb-0">Consultation in Technology and Private Education for Study or Special Case</p>
          </div>
        </div>
      </div>
    </section>

    <!-- Portfolio -->
	<!--
    <section class="content-section" id="portfolio">
      <div class="container">
        <div class="content-section-heading text-center">
          <h3 class="text-secondary mb-0">Portfolio</h3>
          <h2 class="mb-5">Recent Projects</h2>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-3">
            <a class="portfolio-item" href="#">
              <img class="img-fluid" src="<?=base_url();?>assets/theme/img/portfolio-1.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-3">
            <a class="portfolio-item" href="#">
              <img class="img-fluid" src="<?=base_url();?>assets/theme/img/portfolio-2.jpg" alt="">
            </a>
          </div>
		  <div class="col-lg-3">
            <a class="portfolio-item" href="#">
              <img class="img-fluid" src="<?=base_url();?>assets/theme/img/portfolio-2.jpg" alt="">
            </a>
          </div>
		  <div class="col-lg-3">
            <a class="portfolio-item" href="#">
              <img class="img-fluid" src="<?=base_url();?>assets/theme/img/portfolio-2.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-6">
            <a class="portfolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Strawberries</h2>
                  <p class="mb-0">Strawberries are such a tasty snack, especially with a little sugar on top!</p>
                </span>
              </span>
              <img class="img-fluid" src="<?=base_url();?>assets/theme/img/portfolio-3.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-6">
            <a class="portfolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Workspace</h2>
                  <p class="mb-0">A yellow workspace with some scissors, pencils, and other objects.</p>
                </span>
              </span>
              <img class="img-fluid" src="<?=base_url();?>assets/theme/img/portfolio-4.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
    </section>
	
	-->

    <!-- Map -->
    <section id="contact" class="map">
      <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.3905311547082!2d106.80864285015414!3d-6.598291566312695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c5935743b7e3%3A0x76074157a5981127!2si7%20Creative%20Community%20Space!5e0!3m2!1sen!2sid!4v1646508701681!5m2!1sen!2sid"></iframe>
      <br/>
      <small>
        <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.3905311547082!2d106.80864285015414!3d-6.598291566312695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c5935743b7e3%3A0x76074157a5981127!2si7%20Creative%20Community%20Space!5e0!3m2!1sen!2sid!4v1646508701681!5m2!1sen!2sid"></a>
      </small>
    </section>

    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
		<p><b>A</b>: i7 Creative Community Space - Jl. Baranangsiang III No.7, RT.05/RW.08, Tegallega, Kecamatan Bogor Tengah, Kota Bogor, Jawa Barat 16129</p>
		<!-- <p><b>A</b>: Bambu Wulung 1 No.20 Rt.03 Rw.05 Kel.Bambu Apus, Kec.Cipayung Jaktim - 13890</p> -->
		<p><b>P</b>: +62 812 9588 6962 (Eja) </p>
        <ul class="list-inline mb-5">
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="https://instagram.com/derucode">
              <i class="icon-social-instagram"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white" href="https://twitter.com/derucode">
              <i class="icon-social-twitter"></i>
            </a>
          </li>
        </ul>
        <p class="text-muted small mb-0">Copyright &copy; Derucode Asrifia Indonesia</p>
      </div>
    </footer>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url();?>assets/theme/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url();?>assets/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?=base_url();?>assets/theme/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?=base_url();?>assets/theme/js/stylish-portfolio.min.js"></script>

  </body>

</html>
